from Producto import Producto
from Orden import Orden


producto1 = Producto('Camisa', 100.00)
producto2 = Producto('Pantalon', 150.00)
producto3 = Producto('Calcetines', 50.00)
producto4 = Producto('Blusa', 70.00)
productos = [producto1, producto2]
productos2 = [producto3, producto4]
orden = Orden(productos)
orden.agregar(producto3)
orden.agregar(producto4)
print(orden)
print(f'Total de orden: {orden.total()}')
orden2 = Orden(productos2)
print(orden2)
print(f'Total de orden: {orden2.total()}')