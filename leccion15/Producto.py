class Producto:
    contador = 0

    def __init__(self, nombre, precio):
        Producto.contador += 1
        self._id = Producto.contador
        self._nombre = nombre
        self._precio = precio

    @property
    def nombre(self):
        return self._nombre

    @nombre.setter
    def nombre(self, nombre):
        self._nombre = nombre

    @property
    def precio(self):
        return self._precio

    @precio.setter
    def precio(self, precio):
        self._precio = precio

    def __str__(self):
        return f'Id: {self._id}, Nombre: {self._nombre}, Precio: {self._precio}'


if __name__ == '__main__':
    producto = Producto('Camisa', 100.00)
    print(producto)
    producto2 = Producto('Pantalon', 150.00)
    print(producto2)