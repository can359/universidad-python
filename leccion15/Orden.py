from Producto import Producto

class Orden:
    contador = 0

    def __init__(self, productos):
        Orden.contador += 1
        self._id = Orden.contador
        self._productos = list(productos)

    def agregar(self, producto):
        self._productos.append(producto)

    def total(self):
        total = 0
        for producto in self._productos:
            total += producto.precio
        return total

    def __str__(self):
        productos_str = ''
        for producto in self._productos:
            productos_str += producto.__str__() + '|'
        return f'Orden: {self._id}, \nProductos: {productos_str}'

if __name__ == '__main__':
    producto1 = Producto('Camisa', 100.00)
    producto2 = Producto('Pantalon', 150.00)
    productos = [producto1, producto2]
    orden = Orden(productos)
    print(orden)
    orden2 = Orden(productos)
    print(orden2)