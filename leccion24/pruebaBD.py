import psycopg2

conexion = psycopg2.connect(
    user='admin',
    password='admin1235',
    host='127.0.0.1',
    port='5432',
    database='universidad_python'
)

try:
    with conexion:
        with conexion.cursor() as cursor:
            sentencia = 'DELETE FROM personas where id = %s'
            valores = (
                (4,),
                (5,)
            )
            cursor.executemany(sentencia, valores)
            registros = cursor.rowcount
            print(f'Registros eliminados: {registros}')
except Exception as e:
    print(f'Ocurrio un error: {e}')
finally:
    conexion.close()