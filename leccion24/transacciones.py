import psycopg2 as bd

conexion = bd.connect(
    user='admin',
    password='admin1235',
    host='127.0.0.1',
    port='5432',
    database='universidad_python'
)

try:
    with conexion:
        with conexion.cursor() as cursor:
            sentencia = 'INSERT INTO personas(nombre, apellido, email) VALUES(%s, %s, %s)'
            valores = ('Maggie', 'Simpson', 'magsimpson@gmail.com')
            cursor.execute(sentencia, valores)

            sentencia = 'UPDATE personas SET nombre = %s WHERE id = %s'
            valores = ('Homero', 10)
            cursor.execute(sentencia, valores)

            print('Termina la transaccion')
except Exception as e:
    conexion.rollback()
    print(f'Ocurrio un error: {e}')
finally:
    conexion.close()