from Persona import Persona

print('Creacion de objetos'.center(50, '-'))
persona = Persona('Marge', 'Simpson', 45)
persona.informacion()

print('Eliminacion de objetos'.center(50, '-'))
del persona