# Leer contenido online
import urllib
from urllib.request import urlopen

peticion = urllib.request.Request(
    'http://globalmentoring.com.mx/recursos/GlobalMentoring.txt',
    data=None,
    headers={
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
    }
)

with urlopen(peticion) as mensaje:
    contenido = mensaje.read().decode('UTF-8')
    print(contenido)

with open('sitio.txt', 'w', encoding='utf-8') as archivo:
    archivo.write(contenido)

palabras = []
with urlopen(peticion) as mensaje:
    for linea in mensaje:
        palabras_por_linea = linea.decode('utf-8').split()
        for palabra in palabras_por_linea:
            palabras.append(palabra)
print(palabras)

# Contar ocurrencias de una cadena
print('No. veces Universidad: ', contenido.count('Universidad'))
# Upper convierte a mayusculas un str
print(contenido.upper())
print(contenido)
# Lower convierte a minusculas un str
print(contenido.lower())
# Python se encuentra dentro del contenido
print('Existe python?:', 'python' in contenido)
print('Existe python (lower)?:', 'python'.lower() in contenido.lower())
print('Existe python (uppder)?:', 'python'.upper() in contenido.upper())
# Startswith - inicia con
print('Inicia con: ',contenido.startswith('En globalmentoring.com.mx'))
# Startswith - inicia con
print('Termina con:', contenido.lower().endswith('globalmentoring.com.mx'.lower()))

mensaje = 'Hola Mundo'
print('Contiene solo minusculas?: ', mensaje.lower().islower())
print('Contiene solo mayusculas?:', mensaje.upper().isupper())

# Alinear cadenas
# Centrar cadena - center
titulo = 'Sitio Web'
print(len(titulo))
print(titulo.center(50, '*'))
print(len(titulo.center(50, '*')))
print(titulo.center(len(titulo)+10, '-'))
# Justifica a la izquierda
print(titulo.ljust(50, '*'))
print(titulo.ljust(len(titulo)+10, '-'))
# Justifica a la derecha
print(titulo.rjust(50, '*'))
print(titulo.rjust(len(titulo)+10, '-'))

# Reemplazar contenido en un str
print(contenido.replace(' ', '-'))
# Eliminar caracteres al inicio y final de un str - strip
titulo = ' *** SitioWeb *** '
print('Variable original:', titulo)
titulo = titulo.strip()
print('Cadena modificada:', titulo)
titulo = '***SitioWeb***'.strip('*')
print('Cadena modificada:', titulo)
titulo = '***SitioWeb***'.lstrip('*')
print('Cadena modificada:', titulo)
titulo = '***SitioWeb***'.rstrip('*')
print('Cadena modificada:', titulo)
titulo = ' *** SitioWeb *** '.strip().strip('*').strip()
print('Cadena modificada:', titulo)
