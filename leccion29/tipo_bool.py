# bool contiene los valores de True y False
# Tipos numericos, False para 0, True demas valores
v = 0.0
r = bool(v)
print(f'Valor: {v}, resultado: {r}')
v = 15.0
r = bool(v)
print(f'Valor: {v}, resultado: {r}')

# Tipo str, False para '', True para demas valores
v = ''
r = bool(v)
print(f'Valor: {v}, resultado: {r}')
v = 'Hola'
r = bool(v)
print(f'Valor: {v}, resultado: {r}')

# Tipo colecciones, False para colecciones vacias, True para todas las demas
v = []
r = bool(v)
print(f'Valor: {v}, resultado: {r}')
v = [1, 5, 0]
r = bool(v)
print(f'Valor: {v}, resultado: {r}')

# Tupla
v = ()
r = bool(v)
print(f'Valor: {v}, resultado: {r}')
v = (1, 5, 0)
r = bool(v)
print(f'Valor: {v}, resultado: {r}')

# Diccionarop
v = {}
r = bool(v)
print(f'Valor: {v}, resultado: {r}')
v = {'n': 'Lisa', 'a': 'Simpson'}
r = bool(v)
print(f'Valor: {v}, resultado: {r}')

if bool(''):
    print('Verdadero')
else:
    print('Falso')

if '':
    print('Verdadero')
else:
    print('Falso')