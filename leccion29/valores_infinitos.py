# Manejo de valores infinitos
import math
from decimal import Decimal

infinito_positivo = float('inf')
infinito_negativo = float('-inf')

# Modulo math
infinito_positivo = math.inf
infinito_negativo = -math.inf

# Modulo decimal
infinito_positivo = Decimal('Infinity')
infinito_negativo = -Decimal('Infinity')

print(f'Infinito positivo: {infinito_positivo}')
print(f'Es infinito?: {math.isinf(infinito_positivo)}')
print(f'Infinito negativo: {infinito_negativo}')
print(f'Es infinito?: {math.isinf(infinito_negativo)}')