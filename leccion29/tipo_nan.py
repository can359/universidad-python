import math
from decimal import Decimal

# NaN - Not a Number
# NaN - no es case sensitive
# NaN - tipo de dato numerico indefinido

# Con float
a = float('NaN')

# Con decimal
a = Decimal('NaN')

print(f'a: {a}')
print(f'Es NaN?: {math.isnan(a)}')