class MiClase:
    '''
    Este es un ejemplo de la documentacion
    de nuestra clase
    '''

    def __init__(self):
        '''
        Metodo de inicio
        de nuestra clase
        '''

    def metodo(self, param1, param2):
        """Esta es la documentacion del metodo

        Args:
            param1 (Any): Este es el parametro1
            param2 (Any): Este es el parametro2
        """