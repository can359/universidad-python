from mi_clase import MiClase
# Profundizar en tipo str

# Concatenacion automatica
# v = 'Adios'
# m = 'Hola' 'Mundo' + v
# m += 'Universidad' ' Python'

# help(MiClase)
# print(MiClase.__doc__)
# print(MiClase.__init__.__doc__)
# print(MiClase.metodo.__doc__)
# print(MiClase.metodo)
# print(type(MiClase.metodo))

# m1 = 'hola mundo'
# m2 = m1.capitalize()
# print(f'Mensaje1: {m1}, id: {hex(id(m1))}')
# print(f'Mensaje2: {m2}, id: {hex(id(m2))}')
# m1 += 'adios'
# print(f'Mensaje1: {m1}, id: {hex(id(m1))}')

# help(str.join)

# t = ('Hola', 'Mundo', 'Universidad', 'Python')
# m = ' '.join(t)
# print(f'Mensaje: {m}')

# l = ['Java', 'Python', 'Angular', 'Spring']
# m = ', '.join(l)
# print(f'Mensaje: {m}')

# c = 'HolaMundo'
# m = '.'.join(c)
# print(f'Mensaje: {m}')

# d = {'nombre': 'Lisa', 'apellido': 'Simpson', 'edad': '20'}
# ll = '-'.join(d.keys())
# v = '-'.join(d.values())
# print(f'Llaves: {ll}, type: {type(ll)}')
# print(f'Values: {v}, type: {type(v)}')

# help(str.split)
# c = 'Java Python JavaScript Angular Spring Excel'
# l = c.split(' ')
# print(f'lista: {l}, tipo: {type(l)}')

# c = 'Java,Python,JavaScript,Angular,Spring,Excel'
# l = c.split(',')
# print(f'lista: {l}, tipo: {type(l)}')

# Dar formato a un str
# nombre = 'Lisa'
# edad = 28
# m_con_formato = 'Mi nombre es %s y tengo %d años'%(nombre, edad)
# print(m_con_formato)
# p = ('Maggie', 'Simpson', 5000.00)
# m_con_formato = 'Hola %s %s. Tu sueldo es %.2f'%p
# print(m_con_formato)
# m_con_formato = 'Hola %s %s. Tu sueldo es %.2f'
# print(m_con_formato%p)

# nombre = 'Lisa'
# edad = 28
# sueldo = 3000
# m_con_formato = 'Nombre {} Edad {} Sueldo {:.2f}'.format(nombre, edad, sueldo)
# print(m_con_formato)

# m_con_formato = 'Nombre {0} Edad {1} Sueldo {2:.2f}'.format(nombre, edad, sueldo)
# print(m_con_formato)

# m_con_formato = 'Sueldo {2:.2f} Edad {1} Nombre {0}'.format(nombre, edad, sueldo)
# print(m_con_formato)

# m_con_formato = 'Nombre {n} Edad {e} Sueldo {s:.2f}'.format(n=nombre, e=edad, s=sueldo)
# print(m_con_formato)

# p = {'nombre': 'Bart'}
# m_con_formato = 'Nombre {p[nombre]}'.format(p=p)
# print(m_con_formato)
# print(f'Nombre {nombre}')

# Multiplicacion str
# r = 3*'Hola'
# print(f'Resultado: {r}')

# Multiplicacion tuplas
# r = 3*('Hola', 10)
# print(f'Resultado: {r}')

# Multiplicacion listas
# r = 3*[0]
# print(f'Resultado: {r}')

# Caracteres de escape
r = 'Hola \' Mundo '
print(f'Resultado: {r}')

# Caracter \
r = 'c:\\root\\user'
print(f'Resultado: {r}')

# Raw string
r = r'c:\root\user'
print(f'Resultado: {r}')

# caracteres unicode
print('Hola\u0020Mundo')
print('Notacion simple:', '\u0041')
print('Notacion extendida:', '\U00000041')
print('Notacion hexadecimal:', '\x41')
print('Corazon:', '\u2665')
print('Cara sonriendo:', '\U0001f600')
print('Serpiente:', '\U0001F40D')

# caracteres ascii
c = chr(65)
print('A mayuscula:', c)
c = chr(64)
print('Simbolo @:', c)
c = chr(97)
print('A minuscula:', c)

# Caracteres bytes
caracteres_en_bytes = b'Hola Mundo'
print(caracteres_en_bytes)
mensaje = b'Universidad Python'
print(mensaje[1])
print(chr(mensaje[1]))
lista_caracteres = mensaje.split()
print(lista_caracteres)

# Convertir str a bytes
string = 'Programación con Python'
print('string original:', string)
bytes = string.encode('UTF-8')
print('bytes codificado:', bytes)
# Convertir de bytes a str
string2 = bytes.decode('UTF-8')
print('string decodificado:', string2)
print(string == string2)