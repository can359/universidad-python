from Cuadrado import Cuadrado
from Rectangulo import Rectangulo

print('Creacion cuadrado'.center(50, '-'))
cuadrado = Cuadrado(5, 'rojo')
print(f'Calculo area cuadrado: {cuadrado.area()}')
print(cuadrado)

# MRO - Method Resolution Order
print(Cuadrado.mro())

print('Creacion rectangulo'.center(50, '-'))
rec = Rectangulo(3, 8, 'verder')
print(f'Calculo area rectangulo: {rec.area()}')
print(rec)