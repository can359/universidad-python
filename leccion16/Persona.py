class Persona:
    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad

    def __add__(self, otro):
        return f'{self.nombre} {otro.nombre}'

    def __sub__(self, otro):
        return self.edad - otro.edad


p1 = Persona('Lisa', 20)
p2 = Persona('Maggie', 8)

print(p1 + p2)
print(p1 - p2)