# Generador de numeros del 1 al 5
def generador_numeros():
    for numero in range(1, 6):
        yield numero
        print('Se reanuda la ejecucion')

# Utilizamos el generador
gen = generador_numeros()
print(f'Objeto generador: {gen}')
print(type(gen))

# Consumimos los valores del generador
for valor in gen:
    print(f'Numero producido: {valor}')

# Consumir a demanda
gen = generador_numeros()
try:
    print(f'Consumimos a demanda: {next(gen)}')
    print(f'Consumimos a demanda: {next(gen)}')
    print(f'Consumimos a demanda: {next(gen)}')
    print(f'Consumimos a demanda: {next(gen)}')
    print(f'Consumimos a demanda: {next(gen)}')
    print(f'Consumimos a demanda: {next(gen)}')
except StopIteration as e:
    print(f'Error al consumir generador {e}')

# Otra forma de consumir un generador
gen = generador_numeros()
while True:
    try:
        valor = next(gen)
        print(f'Impresion valor generado: {valor}')
    except StopIteration as e:
        print('Se termino de iterar el generador')
        break