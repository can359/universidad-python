import os
from shutil import rmtree

work_dir = os.getcwd()
content = os.listdir(work_dir)

for a in content:
    if os.path.isdir(a) and a != '.git':
        for b in os.scandir(a):
            if b.name == '__pycache__':
                rmtree(f'{work_dir}/{a}/{b.name}')
            elif b.name == 'dominio' or b.name == 'servicio':
                dir = f'{work_dir}/{a}/{b.name}/__pycache__'
                if os.path.isdir(dir):
                    rmtree(dir)

print('Limpieza terminada')