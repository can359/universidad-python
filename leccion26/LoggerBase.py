import logging as log

log.basicConfig(
    level=log.DEBUG,
    format='%(asctime)s: %(levelname)s [%(filename)s:%(lineno)s] %(message)s',
    datefmt='%I:%M:%S %p',
    handlers=[
        log.FileHandler('leccion26/leccion26.log'),
        log.StreamHandler()
    ]
)

if __name__ == '__main__':
    log.debug('Mensaje debug')
    log.info('Mensaje info')
    log.warning('Mensaje warning')
    log.error('Mensaje error')
    log.critical('Mensaje critical')