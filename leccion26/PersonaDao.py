from Conexion import Conexion
from Persona import Persona
from LoggerBase import log
from CursorDelPool import CursorDelPool

class PersonaDao:
    '''
    DAO (Data Access Object)
    CRUD (Create-Read-Update-Delete)
    '''

    _SELECCIONAR = 'SELECT * FROM personas ORDER BY id'
    _INSERTAR = 'INSERT INTO personas(nombre, apellido, email) VALUES(%s, %s, %s)'
    _ACTUALIZAR = 'UPDATE personas SET nombre=%s, apellido=%s, email=%s WHERE id=%s'
    _ELIMINAR = 'DELETE FROM personas WHERE id=%s'

    @classmethod
    def seleccionar(cls):
        with CursorDelPool() as cursor:
            cursor.execute(cls._SELECCIONAR)
            registros = cursor.fetchall()
            personas = []
            for r in registros:
                personas.append(Persona(r[0], r[1], r[2], r[3]))
            return personas

    @classmethod
    def insertar(cls, persona):
        with CursorDelPool() as cursor:
            valores = (persona.nombre, persona.apellido, persona.email)
            cursor.execute(cls._INSERTAR, valores)
            log.debug(f'Persona creada: {persona}')
            return cursor.rowcount

    @classmethod
    def actualizar(cls, persona):
        with CursorDelPool() as cursor:
            valores = (persona.nombre, persona.apellido, persona.email, persona.id)
            cursor.execute(cls._ACTUALIZAR, valores)
            log.debug(f'Persona actualizada: {persona}')
            return cursor.rowcount

    @classmethod
    def eliminar(cls, persona):
        with CursorDelPool() as cursor:
            valores = (persona.id,)
            cursor.execute(cls._ELIMINAR, valores)
            log.debug(f'Persona eliminada {persona}')
            return cursor.rowcount

if __name__ == '__main__':
    # Insertar
    p = Persona(nombre='Bart', apellido='Simpson', email='bsimpson@gmail.com')
    pi = PersonaDao.insertar(p)
    log.debug(f'Personas creadas: {pi}')

    # Actualizar
    # p = Persona(8, 'Mickey', 'Mouse', 'mmouse@gmail.com')
    # pa = PersonaDao.actualizar(p)
    # log.debug(f'Personas actualizadas: {pa}')

    # Eliminar
    # p = Persona(id=11)
    # pe = PersonaDao.eliminar(p)
    # log.debug(f'Personas eliminadas {pe}')

    # Seleccionar
    resultados = PersonaDao.seleccionar()
    for p in resultados:
        log.debug(p)