from LoggerBase import log
from UsuarioDao import UsuarioDao
from Usuario import Usuario

opcion = None
while opcion != 5:
    print(f'''Opciones:
    1. Listar usuarios
    2. Agregar usuario
    3. Modificar usuario
    4. Eliminar usuario
    5. Salir
    ''')
    opcion = int(input('Escribe tu opcion (1-5): '))
    if opcion == 1:
        usuarios = UsuarioDao.seleccionar()
        for u in usuarios:
            log.info(u)
    elif opcion == 2:
        username = input('Escribe el username: ')
        password = input('Escribe el password: ')
        usuario = Usuario(username=username, password=password)
        result = UsuarioDao.insertar(usuario)
        log.info(f'Usuarios insertados: {result}')
    elif opcion == 3:
        id = int(input('Escribe el id a modificar: '))
        username = input('Escribe el username: ')
        password = input('Escribe el password: ')
        usuario = Usuario(id, username, password)
        result = UsuarioDao.actualizar(usuario)
        log.info(f'Usuarios actualizados: {result}')
    elif opcion == 4:
        id = int(input('Escribe el id a eliminar: '))
        usuario = Usuario(id=id)
        result = UsuarioDao.eliminar(usuario)
        log.info(f'Usuarios eliminados: {result}')
else:
    log.info('Salimos de la aplicacion')