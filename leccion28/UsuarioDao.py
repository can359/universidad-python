from LoggerBase import log
from CursorDelPool import CursorDelPool
from Usuario import Usuario

class UsuarioDao:
    '''
    DAO - Data Access Object para la tabla de usuarios
    CRUD - Create, Read, Update, Delete para la tabla usuarios
    '''

    _SELECCIONAR = 'SELECT * FROM usuarios ORDER BY id'
    _INSERTAR = 'INSERT INTO usuarios(username, password) VALUES(%s, %s)'
    _ACTUALIZAR = 'UPDATE usuarios SET username=%s, password=%s WHERE id=%s'
    _ELIMINAR = 'DELETE FROM usuarios WHERE id=%s'

    @classmethod
    def seleccionar(cls):
        with CursorDelPool() as cursor:
            log.debug('Seleccionando usuarios')
            cursor.execute(cls._SELECCIONAR)
            registros = cursor.fetchall()
            usuarios = []
            for r in registros:
                usuarios.append(Usuario(r[0], r[1], r[2]))
            return usuarios

    @classmethod
    def insertar(cls, usuario):
        with CursorDelPool() as cursor:
            log.debug(f'Usuario a crear: {usuario}')
            valores = (usuario.username, usuario.password)
            cursor.execute(cls._INSERTAR, valores)
            return cursor.rowcount

    @classmethod
    def actualizar(cls, usuario):
        with CursorDelPool() as cursor:
            log.debug(f'Usuario a actualizar: {usuario}')
            valores = (usuario.username, usuario.password, usuario.id)
            cursor.execute(cls._ACTUALIZAR, valores)
            return cursor.rowcount

    @classmethod
    def eliminar(cls, usuario):
        with CursorDelPool() as cursor:
            log.debug(f'Usuario a eliminar: {usuario}')
            valores = (usuario.id,)
            cursor.execute(cls._ELIMINAR, valores)
            return cursor.rowcount

if __name__ == '__main__':
    # Insertar
    u = Usuario(username='bsimpson', password='Pandas23*')
    ui = UsuarioDao.insertar(u)
    log.debug(f'Usuarios creados: {ui}')

    # Actualizar
    u = Usuario(1, 'bsimpsons', 'pandas23*')
    ua = UsuarioDao.actualizar(u)
    log.debug(f'Usuarios actualizados: {ua}')

    # Eliminar
    u = Usuario(id=1)
    ue = UsuarioDao.eliminar(u)
    log.debug(f'Usuarios eliminados: {ue}')

    # Seleccionar
    resultados = UsuarioDao.seleccionar()
    for u in resultados:
        log.debug(u)