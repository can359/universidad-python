from LoggerBase import log
from psycopg2 import pool
import sys

class Conexion:

    _DATABASE = 'universidad_python'
    _USERNAME = 'admin'
    _PASSWORD = 'admin1235'
    _PORT = '5432'
    _HOST = '127.0.0.1'
    _MIN_CON = 1
    _MAX_CON = 5
    _pool = None

    @classmethod
    def obtenerPool(cls):
        if cls._pool is None:
            try:
                cls._pool = pool.SimpleConnectionPool(
                    cls._MIN_CON,
                    cls._MAX_CON,
                    user=cls._USERNAME,
                    password=cls._PASSWORD,
                    host=cls._HOST,
                    port=cls._PORT,
                    database=cls._DATABASE
                )
                log.debug(f'Creacion del pool exitosa: {cls._pool}')
                return cls._pool
            except Exception as e:
                log.error(f'Ocurrio un error al obtener el pool: {e}')
                sys.exit()
        else:
            return cls._pool

    @classmethod
    def obtenerConexion(cls):
        conexion = cls.obtenerPool().getconn()
        log.debug(f'Conexion obtenida del pool: {conexion}')
        return conexion

    @classmethod
    def liberarConexion(cls, conexion):
        cls.obtenerPool().putconn(conexion)
        log.debug(f'Regresamos la conexion al pool: {conexion}')

    @classmethod
    def cerrarConexiones(cls):
        cls.obtenerPool().closeall()
        log.debug('Cerrar todos los pool')

if __name__ == '__main__':
    c = Conexion.obtenerConexion()
    Conexion.liberarConexion(c)
    c2 = Conexion.obtenerConexion()
    c3 = Conexion.obtenerConexion()
    Conexion.liberarConexion(c3)
    c4 = Conexion.obtenerConexion()
    c5 = Conexion.obtenerConexion()
    Conexion.liberarConexion(c5)
    c6 = Conexion.obtenerConexion()