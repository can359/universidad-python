# Un closure es una funcion que define a otra, y ademas la puede regresar
# la funcion anidada puede acceder a las variables locales definidas
# en la funcion principal o externa

# Funcion principal
# def operacion (a, b):
#     # 1. Definimos una funcion interna o anidada
#     def sumar():
#         return a + b

#     # 2. Retornar la funcion
#     return sumar

# Funcion principal
def operacion(a, b):
    # 1. Definimos una funcion lambda interna o anidada y la retornamos
    return lambda: a + b

funcion_closure = operacion(5, 2)
print(f'Resultado de la funcion closure: {funcion_closure()}')

# Llamar la funcion regresada al vuelo
print(f'Resultado de la funcion closure al vuelo: {operacion(2, 3)()}')