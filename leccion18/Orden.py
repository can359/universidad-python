class Orden:
    contador = 0

    def __init__(self, computadoras):
        Orden.contador += 1
        self._id = Orden.contador
        self._computadoras = computadoras

    def agregar(self, computadora):
        self._computadoras.append(computadora)

    def __str__(self):
        computadora_str = ''
        for computadora in self._computadoras:
            computadora_str += computadora.__str__()

        return f'''
        Orden: {self._id}
        Computadoras: {computadora_str}
        '''