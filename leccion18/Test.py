from Teclado import Teclado
from Raton import Raton
from Monitor import Monitor
from Computadora import Computadora
from Orden import Orden

t = Teclado('HP', 'USB')
r = Raton('HP', 'USB')
m = Monitor('HP', 24)
c = Computadora('HP', m, t, r)
c2 = Computadora('Build', m, t, r)
c3 = Computadora('Acer', m, t, r)
cs = [c, c2]
o = Orden(cs)
o.agregar(c3)
print(o)
