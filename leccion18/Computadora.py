from Raton import Raton
from Teclado import Teclado
from Monitor import Monitor

class Computadora:
    contador = 0

    def __init__(self, nombre, monitor, teclado, raton):
        Computadora.contador += 1
        self._id = Computadora.contador
        self._nombre = nombre
        self._monitor = monitor
        self._teclado = teclado
        self._raton = raton

    def __str__(self):
        return f'''
            {self._nombre}: {self._id}
                Monitor: {self._monitor}
                Teclado: {self._teclado}
                Raton: {self._raton}
        '''

if __name__ == '__main__':
    t = Teclado('HP', 'USB')
    r = Raton('HP', 'USB')
    m = Monitor('HP', 24)
    c = Computadora('HP', m, t, r)
    print(c)
    c2 = Computadora('Build', m, t, r)
    print(c2)