class Monitor:
    contador = 0

    def __init__(self, marca, tamano):
        Monitor.contador += 1
        self._id = Monitor.contador
        self._marca = marca
        self._tamano = tamano

    def __str__(self):
        return f'Id: {self._id}, Marca: {self._marca}, Tamaño: {self._tamano}'

if __name__ == '__main__':
    m = Monitor('Sceptre', 24)
    m2 = Monitor('Sceptre', 22)
    print(m)
    print(m2)