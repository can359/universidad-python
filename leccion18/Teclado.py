from DispositivoEntrada import DispositivoEntrada

class Teclado(DispositivoEntrada):
    contador = 0

    def __init__(self, marca, tipo):
        Teclado.contador += 1
        self._id = Teclado.contador
        super().__init__(marca, tipo)

    def __str__(self):
        return f'Id: {self._id}, Marca: {self._marca}, Tipo_entrada: {self._tipo}'

if __name__ == '__main__':
    t = Teclado('HP', 'USB')
    t2 = Teclado('Corsair', 'Bluetooh')
    print(t)
    print(t2)