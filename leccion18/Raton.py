from DispositivoEntrada import DispositivoEntrada

class Raton(DispositivoEntrada):
    contador = 0

    def __init__(self, marca, tipo):
        Raton.contador += 1
        self._id = Raton.contador
        super().__init__(marca, tipo)

    def __str__(self):
        return f'Id: {self._id}, Marca: {self._marca}, Tipo_entrada: {self._tipo}'

if __name__ == '__main__':
    r = Raton('HP', 'USB')
    r2 = Raton('Corsair', 'Bluetooh')
    print(r)
    print(r2)