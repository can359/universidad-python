import os

class CatalogoPeliculas:
    archivo = 'leccion22/peliculas.txt'

    @classmethod
    def agregar(cls, pelicula):
        with open(cls.archivo, 'a', encoding='utf8') as a:
            a.write(f'{pelicula.nombre}\n')

    @classmethod
    def listar(cls):
        with open(cls.archivo, 'r', encoding='utf8') as a:
            print('Catalogo de peliculas'.center(50, '-'))
            print(a.read())

    @classmethod
    def eliminar(cls):
        os.remove(cls.archivo)
        print(f'Archivo eliminado: {cls.archivo}')