class MiClase:

    variable_clase = 'Valor variable clase'

    def __init__(self, variable_instancia):
        self.variable_instancia = variable_instancia

    @staticmethod
    def estatico():
        print(MiClase.variable_clase)

    @classmethod
    def clase(cls):
        print(cls.variable_clase)

    def instancia(self):
        self.clase()
        self.estatico()
        print(self.variable_clase)
        print(self.variable_instancia)

MiClase.clase()
obj = MiClase('variable_instancia')
obj.clase()
obj.instancia()