# Unpacking - desempaquetado
from resource import prlimit


valores = 1, 2, 3
print(valores)
print(type(valores))

v1, v2, v3 = 1, 2, 3
print(v1, v2, v3)

v1, _, v3 = 1, 2, 3
print(v1, v3)

v1, v2, *v3 = 1, 2, 3, 4, 5, 6, 7, 8, 9
print(v1, v2, v3)

v1, v2, *v3, v4, v5 = 1, 2, 3, 4, 5, 6, 7, 8, 9
print(v1, v2, v3, v4, v5)


v1, v2, *v3, v4, v5 = [1, 2, 3, 4, 5, 6, 7, 8, 9]
print(v1, v2, v3, v4, v5)
print(type(v3))

def regresa_varios_datos():
    return (1, 2, 3)

v1, v2, v3 = regresa_varios_datos()
print(v1, v2, v3)

v1, *v_restantes = regresa_varios_datos()
print(v1, v_restantes)

# help(str.partition)
hora, _, minutos = '17:20'.partition(':')
print(hora, minutos)