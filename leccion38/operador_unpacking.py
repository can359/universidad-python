# * desempaquetar
numeros = [1, 2, 3]
print(numeros)
print(*numeros)
print(*numeros, sep=' - ')

# Desempaquetando al momento de pasar un parametro a una funcion
def sumar(a, b, c):
    print(f'Resultado suma: {a + b+ c}')

sumar(*numeros)

# Extraer algunos elementos de una lista
lista = [1, 2, 3, 4, 5, 6]
a, *b, c, d = lista
print(a, b, c, d)

# Unir listas
l1 = [1, 2, 3]
l2 = [4, 5, 6]
l3 = [l1, l2]
print(f'Lista de listas: {l3}')
l3 = [*l1, *l2]
print(f'Unir listas: {l3}')

# Unir diccionarios
d1 = {'A': 1, 'B': 2, 'C': 3}
d2 = {'D': 4, 'E': 5}
d3 = {**d1, **d2}
print(f'Unir diccionarios: {d3}')

# Construir una lista a partir de un str
l = [*'HolaMundo']
print(l)
print(*l, sep='')