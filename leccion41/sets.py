# Profundizar en set
# Un set es una coleccion de elementos unicos y es mutable
# Los elementos de un set deben ser inmutables
# conjunto = {[1, 2], [3, 4]}
conjunto = {'Lisa', True, 18.8}
print(conjunto)
# Set vacio
# conjunto = {} # genera un dict vacio
# print(type(conjunto))
# Set vacio correcto
conjunto = set()
print(conjunto)
print(type(conjunto))
# Mutable
conjunto.add('Lisa')
print(conjunto)
# Contiene valores unicos
conjunto.add('Lisa')
print(conjunto)
# Crear un set a partir de un iterable
conjunto = set([4, 5, 7 ,8 ,4])
print(conjunto)
# Podemos agregar mas elementos o incluso otro set
conjunto2 = {100, 200, 300, 300}
conjunto.update(conjunto2)
print(conjunto)
conjunto.update([20, 30 ,40, 40])
print(conjunto)

# Copiar un set (copia poco profunda, solo copia referencias)
conjunto_copia = conjunto.copy()
print(conjunto_copia)
# Verificar igualdad
print(f'Es igual en contenido? {conjunto == conjunto_copia}')
print(f'Es la misma referencia? {conjunto is conjunto_copia}')

# Operaciones de conjuntos con set
# Personas con distintas caracteristicas
pelo_negro = {'Juan', 'Karla', 'Pedro', 'Maria'}
pelo_rubio = {'Lorenzo', 'Laura', 'Marco'}
ojos_cafe = {'Karla', 'Laura'}
menores_30 = {'Juan', 'Karla', 'Maria'}
# Todos con ojos cafe y pelo rubio(Union) (no se repiten los elementos)
print(ojos_cafe.union(pelo_rubio))
# Ivertir el orden con el mismo resultado (conmutativa)
print(pelo_rubio.union(ojos_cafe))

# (intersection) solo las personas con ojos cafe y pelo rubio (conmutativa)
print(ojos_cafe.intersection(pelo_rubio))

# (difference)  pelo negro sin ojos cafe (no es conmutativa)
# Las personas que se encuentran en el primer set pero no en el segundo
print(pelo_negro.difference(ojos_cafe))

# (symetric difference) pelo negro u ojos cafe, pero no ambos (conmutativa)
print(pelo_negro.symmetric_difference(ojos_cafe))

# Preguntar si un set esta contenido en otro (subset)
# revisamos si los elementos del primer set estan contenidos en el segundo set
print(menores_30.issubset(pelo_negro))

# Preguntar si un set contiene a otro ser (superset)
# revisar si los elementos del primer ser estan contenidos en el segundo set
print(menores_30.issuperset(pelo_negro))

# Preguntar si los de pelo negro no tienen pelo rubio (distjoin)
print(pelo_negro.isdisjoint(pelo_rubio))