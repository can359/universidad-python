a = open('leccion20/prueba.txt', 'r', encoding='utf8')
# print(a.read())

# leer algunos caracteres
# print(a.read(5))
# print(a.read(3))

# leer lineas completas
# print(a.readline())
# print(a.readline())

# iterar archivo
# for l in a:
#     print(l)

# leer lineas
# print(a.readlines())

# acceder a una linea
# print(a.readlines()[0])

# copiar archivos
a2 = open('leccion20/copia.txt', 'a')
a2.write(a.read())

a.close()
a2.close()
print('Se han cerrado los archivos')