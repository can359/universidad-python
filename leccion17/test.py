from Empleado import Empleado
from Gerente import Gerente

# Polimorfismo
def imprimir(objeto):
    print(type(objeto))
    print(objeto)
    if isinstance(objeto, Gerente):
        print(objeto.departamento)
    print(objeto.mostrar())

empleado = Empleado('Lisa', 5000)
imprimir(empleado)

gerente = Gerente('Marge', 7000, 'Sistemas')
imprimir(gerente)