# Mas de funciones anidadas y alcance de variables

def funcion_externa():
    variable_local_externa = 'V local externa'

    def funcion_anidada():
        variable_local_anidada = 'V local anidada'

        nonlocal variable_local_externa
        variable_local_externa = 'Nuevo valor v local externa'

    funcion_anidada()

    print(f'Valor variable local externa: {variable_local_externa}')
    # No es posible acceder ana variable local mas interna
    # print(f'Valor variable local anidada: {variable_local_anidada}')

funcion_externa()