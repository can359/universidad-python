class Vehiculo:
    def __init__(self, color, ruedas):
        self.color = color
        self.ruedas = ruedas

    def __str__(self):
        return f'vehiculo de color {self.color}, {self.ruedas} ruedas'

class Coche(Vehiculo):
    def __init__(self, color, ruedas, velocidad):
        super().__init__(color, ruedas)
        self.velocidad = velocidad

    def __str__(self):
        return f'Coche {super().__str__()} y {self.velocidad} km/hr'

class Bicicleta(Vehiculo):
    def __init__(self, color, ruedas, tipo):
        super().__init__(color, ruedas)
        self.tipo = tipo

    def __str__(self):
        return f'Bicicleta {super().__str__()} y tipo {self.tipo}'


coche = Coche('Rojo', 4, 250)
print(coche)
bicicleta = Bicicleta('Azul', 2, 'urbana')
print(bicicleta)